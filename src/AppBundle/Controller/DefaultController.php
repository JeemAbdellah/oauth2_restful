<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    public function publicAction(Request $request){
        return $this->render("web/get.html.twig",array("data"=>"this is some public data !"));
    }

    public function privateAction(Request $request){
        $repo = $this->getDoctrine()->getRepository("AppBundle:Post");
        $data = $repo->findAll();
        return $this->render("web/get.html.twig",array("data"=>$data));
    }

    public function adminAction(Request $request){
        $repo = $this->getDoctrine()->getRepository("AppBundle:User");
        $data = $repo->findAll();
        return $this->render("web/get.html.twig",array("data"=>$data));
    }
    
}
