<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
 

class RestController extends FOSRestController
{
    /**
    @ApiDoc(
    * resource="/Ressources/Public",
    * description="Gets some public data",
    * statusCodes={
    *     200="Successful",
    *     403="Validation errors"
    *   },
    * )
    */
    public function getPublicGetAction(){
        $data = array("data"=>"this is some public data !");
        $view = $this->view($data);
        return $this->handleView($view);
    }
    
    /**
    @ApiDoc(
    * resource="/Ressources/Private",
    * description="Gets some private data for users with role user",
    * statusCodes={
    *     200="Successful",
    *     403="Access denied"
    *   },
    * )
    */
    public function getPrivateGetAction(){
        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw new AccessDeniedException();
        }
        $repo = $this->getDoctrine()->getRepository("AppBundle:Post");
        $data = $repo->findAll();
        $view = $this->view($data);
        return $this->handleView($view);
    }

    /**
    @ApiDoc(
    * resource="/Ressources/Admin",
    * description="Gets some private data for users with role admin",
    * statusCodes={
    *     200="Successful",
    *     403="Access denied"
    *   },
    * )
    */
    public function getAdminGetAction(){
        if (false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }
        $repo = $this->getDoctrine()->getRepository("AppBundle:User");
        $data = $repo->findAll();
        $view = $this->view($data);
        return $this->handleView($view);
    }
    
}
